package assignment3;

/**
 * The board class is an instance of the game state
 * This includes all game history, and state variables
 *
 * @author Joshua Cristol (jmc6683) and Andrew Nat (aan474)
 * @version 1.00 2015-03-04
 */
public class Board
{
   private Peg[] code;

   private String[] responses = new String[Game.NUM_GUESS];

   private static Peg[][] guesses = new Peg[Game.NUM_GUESS][Game.CODE_LENGTH];

   private static int numGuessed = 0;

   /**
    * Returns the history of guesses and responses for the game
    *
    * @Return the string representation of the game history
    */
   public String toString()
   {
      StringBuilder sb = new StringBuilder();
      sb.append("\n");
      for (int i = 0; i < numGuessed; i++)
      {
         for (int j = 0; j < Game.CODE_LENGTH; j += 1)
         {
            sb.append(guesses[i][j]);
            sb.append("");
         }
         sb.append("\n");
      }
      return sb.toString();
   }

   /**
    * Add a guess to the board history
    *
    * @param guess The user's guess
    * @param response The computer's response
    */
   public void addGuess(Peg[] guess, String response)
   {
      guesses[numGuessed] = guess;
      responses[numGuessed] = response;
      numGuessed++;
   }

   /**
    * @return Returns the games secret code
    */
   public Peg[] getCode()
   {
      return code;
   }

   /**
    * @param code The value of the game's secret code
    */
   public void setCode(Peg[] code)
   {
      this.code = code;
   }

   /**
    * @return Returns boolean if the number of guesses is up
    */
   public boolean outOfGuesses()
   {
      return numGuessed >= Game.NUM_GUESS;
   }

   /**
    * @return The number of guesses the user has used
    */
   public int getGuesses()
   {
      return numGuessed;
   }

   /**
    * method that checks the history to see if a guess has already been made
    *
    * @param inputGuess a string input that is a guess
    * @return a boolean whether that guess is in the games history
    */
   public static boolean historyCheck(String inputGuess)
   {
      Peg[] inputGuessPegs;
      inputGuessPegs = Game.toPegs(inputGuess);
      for (int i = 0; i < numGuessed; i++)
      {
         int counter = 0;
         for (int j = 0; j < Game.CODE_LENGTH; j++)
         {
            if (guesses[i][j].pegCompare(inputGuessPegs[j]))
            {
               counter += 1;
            }
         }
         if (counter == Game.CODE_LENGTH)
         {
            return true;
         }
      }
      return false;
   }

   /**
    * ressets the number of guesses to zero
    */
   public void resetNumGuessed()
   {
      numGuessed = 0;
   }
}
