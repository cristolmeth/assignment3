package assignment3;

/**
 * Color class for each peg
 *
 * @author Joshua Cristol (jmc6683) and Andrew Nat (aan474)
 * @version 1.00 2015-03-04
 */
public class Color
{
   private String thisColor;

   /**
    * sets the color
    *
    * @param theColor the desired color given as a string
    */
   public void setColor(String theColor)
   {
      thisColor = theColor;
   }

   /**
    * gets color
    *
    * @return the color
    */
   public String getThisColor()
   {
      return thisColor;
   }

   /**
    * compares one color to another for equality if the strings match then true
    *
    * @param otherColor the other color to compare
    * @return a boolean true if same false otherwise
    */
   public boolean compareColor(Color otherColor)
   {
      if (thisColor.equals(otherColor.getThisColor()))
      {
         return true;
      } else
      {
         return false;
      }
   }

   /**
    * String representation of a color
    *
    * @return a string of the desired color
    */
   public String toString()
   {
      return thisColor;
   }
}
