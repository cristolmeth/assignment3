package assignment3;

import java.util.Random;

/**
 * The computer class is a object that manages the creation of codes as well
 * as the comparison of guesses and codes. Manages all the game logic.
 *
 * @author Joshua Cristol (jmc6683) and Andrew Nat (aan474)
 * @version 1.00 2015-03-04
 */
public class Computer
{
   private enum colors
   {
      NONE, BLACK, WHITE
   }

   /**
    * Generates a new code for the board based on the Game class colors and code
    * length
    *
    * @return The string representation of the generated code
    */
   public Peg[] generateCode()
   {
      Random rand = new Random();
      Peg[] pegs = new Peg[Game.CODE_LENGTH];
      for (int i = 0; i < Game.CODE_LENGTH; i += 1)
      {
         // For each index in the string, randomly select one of the possible colors
         int index = rand.nextInt(Game.colors.length);
         String desiredColor = Game.COLORS_FULL[index];
         Peg newPeg = new Peg(desiredColor);
         pegs[i] = newPeg;
      }
      return pegs;
   }

   /**
    * Checks the number of blacks and white pegs in the guess to code comparison
    *
    * @param code The bboards secret code
    * @param guess The user's guess
    * @return String representation of the number of black and white pegs
    */
   public String checkCode(Peg[] code, Peg[] guess)
   {
      colors[] marker = new colors[Game.CODE_LENGTH];
      // find the black pegs, fist pass aligned only
      for (int i = 0; i < Game.CODE_LENGTH; i++)
      {
         if (code[i].pegCompare(guess[i]))
            marker[i] = colors.BLACK;
         else
            marker[i] = colors.NONE;
      }
      // find the white pegs, second pass O(n^2)
      for (int i = 0; i < Game.CODE_LENGTH; i++)
      {
         // make sure that the peg is not spoken for
         if (marker[i] != colors.BLACK)
         {
            for (int j = 0; j < Game.CODE_LENGTH; j++)
            {
               if (marker[j] == colors.NONE)
               {
                  if (guess[i].pegCompare(code[j]))
                  {
                     // mark the peg as spoken for
                     marker[j] = colors.WHITE;
                     break;
                  }
               }
            }
         }
      }
      return generateResponse(marker);
   }

   /**
    * Generates a reponse based on a marker array
    *
    * @param marker The marker array
    * @return The response string
    */
   private String generateResponse(colors[] marker)
   {
      // counts pegs
      int black = 0;
      int white = 0;
      for (int i = 0; i < marker.length; i++)
      {
         if (marker[i] == colors.WHITE)
            white++;
         else if (marker[i] == colors.BLACK)
            black++;
      }
      // check if the response is empty
      if (black == 0 && white == 0)
         return "Result: No pegs";
      StringBuilder result = new StringBuilder();
      result.append("Result: ");
      if (black > 0)
      {
         result.append(black);
         result.append(" black pegs ");
         if (white > 0)
            result.append("and ");
      }
      if (white > 0)
      {
         result.append(white);
         result.append(" white pegs ");
      }
      return result.toString();
   }
}
