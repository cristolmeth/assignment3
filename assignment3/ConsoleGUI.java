package assignment3;

import javax.swing.*;
import java.util.Arrays;

/**
 * Class that handles the IO through GUI
 *
 * @author Joshua Cristol (jmc6683) and Andrew Nat (aan474)
 * @version 1.00 2015-03-04
 */
public class ConsoleGUI
{
   /**
    * This method prompts the user with a yes no option dialog box to start the game
    */
   public void introPrompt()
   {
      int reply = JOptionPane.showConfirmDialog(null, "Welcome to Mastermind.  Here are the rules.\n\n"
              + "This is a version of the classic board game Mastermind.\n"
              + "The computer will think of a secret code. The code consists of "
              + Game.CODE_LENGTH
              + " colored pegs. \n"
              + "The pegs MUST be one of the colors: "
              + Arrays.toString(Game.COLORS_FULL)
              + "\n"
              + "A color may appear more than once in the code. You try to guess what colored pegs\n"
              + "are in the code and what order they are in.   After you make a valid guess the result\n"
              + "(feedback) will be displayed. The result consists of a black peg for each peg you have\n"
              + "guessed exactly correct (color and position) in your guess.\n"
              + "For each peg in the guess that is the correct color, but is out of position, you get a white peg.\n"
              + "For each peg, which is fully incorrect, you get no feedback. \n\n"
              + "Only the first letter of the color is displayed. B for Blue, R for Red, and so forth. "
              + "When entering\n guesses you only need to enter the first character of each color as a capital letter.\n\n"
              + "You have "
              + Game.CODE_LENGTH
              + " guesses to figure out the secret code or you lose the game.\nAre you ready to play?", "Mastermind", JOptionPane.YES_NO_OPTION);
      if (reply == JOptionPane.YES_OPTION)
      {
      } else
      {
         System.exit(0);
      }
   }

   /**
    * This method gets the input for the game through a Joptionpane and returns the guess as a string
    *
    * @param code The peg array that has the actual secret code, this param is used for debugging purposes
    * @param showCode a boolean that is set to true in debugging mode false otherwise
    * @param guessesLeft An integer representing the number of guesses left in the game
    * @return either the guess, a "", or string that says HISTORY
    * @throws IllegalGuessException an exception thrown if an illegal guess is input
    */
   public String getInput(Peg[] code, boolean showCode, int guessesLeft) throws IllegalGuessException
   {
      String message = guessesLeft + " guesses left.\n What is your next guess? \n" +
              "Enter guess: ";
      if (showCode)
      {
         String attach = "The code is ";
         for (int i = 0; i < code.length; i++)
         {
            attach += code[i];
         }
         message = attach + " " + message;
      }
      String result = JOptionPane.showInputDialog(null, message, "Mastermind", JOptionPane.YES_OPTION);
      if (result == null)
      {
         System.exit(0);
      }
      result = result.toUpperCase();
      if (!validCode(result))
      {
         throw new IllegalGuessException();
      }
      if (result.equals("HISTORY"))
      {
         return "HISTORY";
      }
        /*some logic to see if this was already used and if valid*/
      if (Board.historyCheck(result))
      {
         JOptionPane.showMessageDialog(null, "Hey already been guessed");
         return "";
      } else
      {
         return result;
      }
   }

   /**
    * prints the response to a valid guess with a gui
    *
    * @param guess the guess just made
    * @param respose the computer response to that guess
    */
   public void printResponse(String guess, String respose)
   {
      JOptionPane.showMessageDialog(null, guess + " --> " + respose);
   }

   /**
    * prints all the previous guesses of the game
    *
    * @param board the board being used for this game
    */
   public void printHistory(Board board)
   {
      JOptionPane.showMessageDialog(null, board.toString());
   }

   /**
    * message dialog box that appears when a invalid guess is input
    */
   public void inputError()
   {
      JOptionPane.showMessageDialog(null, "Invalid Guess");
   }

   /**
    * Checks to see if the string input code is valid
    *
    * @param code the code input
    * @return a boolean true if valid false if not valid
    */
   public boolean validCode(String code)
   {
      if (code.equals("HISTORY"))
         return true;
      if (Game.CODE_LENGTH != code.length())
      {
         return false;
      }
      for (int i = 0; i < code.length(); i++)
      {
         boolean found = false;
         for (int j = 0; j < Game.colors.length; j++)
         {
            if (code.charAt(i) == Game.colors[j])
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            return false;
         }
      }
      return true;
   }

   /**
    * displays a message if the game has been won
    */
   public void winMessage()
   {
      JOptionPane.showMessageDialog(null, "You Win");
   }

   /**
    * displays a message if the game has been lost
    */
   public void loseMessage()
   {
      JOptionPane.showMessageDialog(null, "You Lose");
   }

   /**
    * Asks the user if they would like to play again with a gui
    *
    * @return a boolean true if they would like to play again false otherwise
    */
   public boolean playAgain()
   {
      int n = JOptionPane.showConfirmDialog(null, "Are you ready for another game? (Y/N): ", "Mastermind", JOptionPane.YES_NO_OPTION);
      if (n == JOptionPane.YES_OPTION)
      {
         return true;
      }
      if (n == JOptionPane.NO_OPTION)
      {
         return false;
      }
      return false;
   }
}
