package assignment3;

/**
 * The game class is an instance of a game object that manages the board, computer, console, and overall
 * game play.
 *
 * @author Joshua Cristol (jmc6683) and Andrew Nat (aan474)
 * @version 1.00 2015-03-04
 */
public class Game
{
   public static final int NUM_GUESS = 15;

   public static final String[] COLORS_FULL = {"blue", "red", "green",
           "orange", "purple", "purple", "yellow", "maroon"};

   public static char[] colors;// = {'B','R','G','O','P','Y'};

   public static final int CODE_LENGTH = 5;

   public static final boolean SHOW_CODE = false;

   private Board board;

   private Computer comp;

   private boolean runAgain = false;

   private ConsoleGUI user_interface;

   /**
    * Constructs a new game object, initilizes board, computer, and codes
    */
   public Game()
   {
      colors = new char[COLORS_FULL.length];
      for (int i = 0; i < COLORS_FULL.length; i++)
      {
         colors[i] = COLORS_FULL[i].toUpperCase().charAt(0);
      }
      comp = new Computer();
      board = new Board();
      board.setCode(comp.generateCode());
      user_interface = new ConsoleGUI();
   }

   /**
    * Begins the games, and prints all intial output, sets the run again value
    *
    * @param fullIntro Boolean to print the full or condensed intro
    */
   public void run(boolean fullIntro)
   {
      if (fullIntro)//change to full intro
      {
         user_interface.introPrompt();
      }
      boolean done = false;
      boolean won = false;
      while (!done)
      {
         String guess = "";
         try
         {
            guess = user_interface.getInput(board.getCode(), SHOW_CODE, NUM_GUESS
                    - board.getGuesses());
         } catch (IllegalGuessException e)
         {
            user_interface.inputError();
         }
         if (!guess.equals(""))
         {
            Peg[] pegGuess = toPegs(guess);
            // check for input or command
            if (guess.equals("HISTORY"))
            {
               user_interface.printHistory(board);
            } else
            {
               // have the computer generate the reponse
               String response = comp.checkCode(board.getCode(), pegGuess);
               board.addGuess(pegGuess, response);
               user_interface.printResponse(guess, response);
               if (board.outOfGuesses())
                  done = true;
               // did the player win?
               if (response.equals("Result: " + CODE_LENGTH + " black pegs "))
               {
                  // they won
                  won = true;
                  done = true;
               }
            }
         }
      }
      // check for exit commands
      if (won)
      {
         user_interface.winMessage();
      } else
      {
         user_interface.loseMessage();
      }
      board.resetNumGuessed();
      runAgain = user_interface.playAgain();
   }

   /**
    * Returns whether the user has opted for another game
    *
    * @return The boolean to represent the users choice
    */
   public boolean runAgain()
   {
      return runAgain;
   }

   /**
    * utility method used to turn a string input into a peg array
    *
    * @param guess the string input guess
    * @return an array of pegs that is a correct representation of the string guess
    */
   public static Peg[] toPegs(String guess)
   {
      Peg[] result = new Peg[CODE_LENGTH];
      boolean foundLetter = false;
      for (int i = 0; i < CODE_LENGTH; i++)
      {
         for (int j = 0; j < colors.length; j++)
         {
            if ((guess.charAt(i) == colors[j]) && (!foundLetter))
            {
               Peg peg1 = new Peg(COLORS_FULL[j]);
               result[i] = peg1;
               foundLetter = true;
            }
         }
         foundLetter = false;
      }
      return result;
   }
}
