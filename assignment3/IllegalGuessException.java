package assignment3;

/**
 * Exception class that is thrown when the guess is illegal
 *
 * @author Joshua Cristol (jmc6683) and Andrew Nat (aan474)
 * @version 1.00 2015-03-04
 */
public class IllegalGuessException extends Exception
{
   /**
    * constructor for the illegal guess exception
    */
   public IllegalGuessException()
   {
      super();
   }
}
