package assignment3;

/**
 * Main driver that runs the program and creates the game section number: 16010
 *
 * @author Joshua Cristol (jmc6683) and Andrew Nat (aan474)
 * @version 1.00 2015-03-04
 */
public class MastermindDriver
{
   public static void main(String[] args)
   {
      boolean cont = true;
      int gameCount = 0;
      while (cont)
      {
         Game g = new Game();
         g.run(gameCount == 0);
         cont = g.runAgain();
         gameCount++;
      }
      System.exit(0);
   }
}
