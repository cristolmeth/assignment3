package assignment3;

/**
 * Peg class that is used as pieces on the board
 *
 * @author Joshua Cristol (jmc6683) and Andrew Nat (aan474)
 * @version 1.00 2015-03-04
 */
public class Peg
{
   private Color pegColor;

   /**
    * peg constructor sets the peg color with the desired color string
    *
    * @param desiredColor
    */
   public Peg(String desiredColor)
   {
      pegColor = new Color();
      pegColor.setColor(desiredColor);
   }

   /**
    * gets the peg color
    *
    * @return returns the peg color
    */
   public Color getPegColor()
   {
      return pegColor;
   }

   /**
    * compares to pegs
    *
    * @param otherPeg the other peg to compare with this peg
    * @return boolean true if same false otherwise
    */
   public boolean pegCompare(Peg otherPeg)
   {
      if (this.pegColor.compareColor(otherPeg.getPegColor()))
      {
         return true;
      }
      return false;
   }

   /**
    * string representation of a peg
    *
    * @return the string that represents a peg
    */
   public String toString()
   {
      String firstCharecter = pegColor.toString().substring(0, 1).toUpperCase();
      return firstCharecter;
   }
}
